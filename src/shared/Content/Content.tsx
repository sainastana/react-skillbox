import {hot} from 'react-hot-loader/root';
import * as React from 'react';
import styles from './content.css';

interface IcontentProps{
    children?: React.ReactNode;
}
function ContentComponent({children}:IcontentProps){
    return(
        <div className={styles.content}>
           {children}
        </div>
    )
}

export const Content = hot(ContentComponent);