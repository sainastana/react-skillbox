import {hot} from 'react-hot-loader/root';
import * as React from 'react';
import styles from './CardList.css';
import {Card} from './Card/Card';

function CardListComponent(){
    return(
        <ul className={styles.cardsList}>
           <Card />
        </ul>
    )
}

export const CardList = hot(CardListComponent);