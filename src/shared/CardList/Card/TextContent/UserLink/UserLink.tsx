import {hot} from 'react-hot-loader/root';
import * as React from 'react';
import styles from './userLink.css';



function UserLinkComponent(){
    return(
        <div className={styles.userLink}>
            <img className={styles.avatar}
            src="https://cdn.dribbble.com/users/32512/avatars/mini/b394be60a32a4c82f89c73b0349af025.png?1556087466"
            />
            <a href="#user-url" className={styles.username}>Дмитрий Гришин</a>
        </div>
    )
}

export const UserLink = hot(UserLinkComponent);