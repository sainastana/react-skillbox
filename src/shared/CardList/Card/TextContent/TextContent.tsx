import {hot} from 'react-hot-loader/root';
import * as React from 'react';
import styles from './textContent.css';
import {UserLink} from './UserLink/UserLink';



function TextContentComponent(){
    return(
        <div className={styles.textContent}>
            <div className={styles.metaData}>
                <UserLink/>
                <span className={styles.creaateAt}>
                    <span className={styles.publishedLabel}>Опубликовано </span>
                    4 часа назад
                </span> 
            </div>
            <h2 className={styles.title}>
                <a href="#post-url" className={styles.postLink}>
                    Реализация намеченных плановых заданий
                </a>
            </h2>
        </div>
    )
}

export const TextContent = hot(TextContentComponent);