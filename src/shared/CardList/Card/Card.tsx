import {hot} from 'react-hot-loader/root';
import * as React from 'react';
import styles from './Card.css';
import {CardMenu} from './CardMenu/CardMenu';
import {TextContent} from './TextContent/TextContent';
import {KarmaCounter} from './KarmaCounter/KarmaCounter';
import {CommentsButton} from './CommentsButton/CommentsButton';

function CardComponent(){
    return(
        <li className={styles.card}>
            <TextContent/>
            <div className={styles.preview}>
                <img className={styles.previewImg} 
                src="https://cdn.dribbble.com/userupload/2918812/file/original-b2186a1651198990a4cf5288b44b9336.png?compress=1&resize=320x240&vertical=top"
                />
            </div>
            <CardMenu/>
            <div className={styles.controls}>
                <KarmaCounter/>
                <CommentsButton/>
            </div>        
        </li>
    )
}

export const Card = hot(CardComponent);