import React from 'react';
import {hot} from 'react-hot-loader/root';
import './main.global.css';
import {Layout} from './shared/Layout/Layout';
import {Header} from './shared/Header/Header';
import {Content} from './shared/Content/Content';
import {CardList} from './shared/CardList/CardList';
// import {Card} from './shared/CardList/Card/Card';

export function AppComponent(){
    return(
        <Layout>
          <Header></Header>
          <Content>
            <CardList></CardList>
          </Content>
        </Layout>
    )
}

export const App = hot(AppComponent);